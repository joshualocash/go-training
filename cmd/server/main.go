package main

import (
	"log"

	"bitbucket.org/joshualocash/go-training/internal/server"
)

func main() {
	if err := server.Run(); err != nil {
		log.Fatal(err)
	}
}
