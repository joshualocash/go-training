SHELL := /bin/bash

all:
	go build -o build/server ./cmd/server
vet: fmt
	go vet ./...
fmt:
	go fmt ./...
