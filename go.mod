module bitbucket.org/joshualocash/go-training

go 1.13

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.1.1
	github.com/jinzhu/gorm v1.9.12
	github.com/lib/pq v1.1.1
	github.com/spf13/cobra v1.0.0
	google.golang.org/protobuf v1.24.0 // indirect
)
