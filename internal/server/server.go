package server

import (
	"fmt"

	"bitbucket.org/joshualocash/go-training/internal/database"
	"bitbucket.org/joshualocash/go-training/internal/server/api"
	"github.com/gin-gonic/gin"
)

type routeHandler func(*gin.Engine, *database.ORM)

func Run() error {
	orm, err := database.Connect(
		database.WithHost("localhost"),
		database.WithPassword("postgres"),
	)

	if err != nil {
		return fmt.Errorf("failed to establish a database connection: %v", err)
	}

	if err := orm.Migrate(); err != nil {
		return fmt.Errorf("failed to perform database migration: %v", err)
	}

	router := gin.Default()
	handlers := []routeHandler{
		api.RouteVM,
	}

	for _, h := range handlers {
		h(router, orm)
	}

	router.Run(":8080")
	return nil
}
