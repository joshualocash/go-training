package api

import (
	"fmt"
	"net/http"

	"bitbucket.org/joshualocash/go-training/internal/database"
	"bitbucket.org/joshualocash/go-training/internal/database/models"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

const (
	errMissingParam   = "paramter '%s' not provided"
	errVMQueryFailed  = "error during VM query: %v"
	errVMCreateFailed = "VM create failed: %v"
	errVMDeleteFailed = "VM delete failed: %v"
)

type uuidRequest struct {
	UUID string `uri:"uuid" binding:"required,uuid"`
}

type vmCreateRequest struct {
	Name string `form:"name" binding:"required"`
}

func RouteVM(router *gin.Engine, orm *database.ORM) {
	router.GET("/vms", func(c *gin.Context) {
		VMList(orm, c)
	})

	router.GET("/vms/:uuid", func(c *gin.Context) {
		VMGet(orm, c)
	})

	router.POST("/vms", func(c *gin.Context) {
		VMCreate(orm, c)
	})

	router.DELETE("/vms/:uuid", func(c *gin.Context) {
		VMDelete(orm, c)
	})
}

func VMList(orm *database.ORM, c *gin.Context) {
	var vms []models.VM
	if err := orm.Find(&vms).Error; err != nil {
		c.JSON(http.StatusOK, gin.H{
			"error": fmt.Sprintf(errVMQueryFailed, err),
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": vms,
	})
}

func VMCreate(orm *database.ORM, c *gin.Context) {
	var req vmCreateRequest
	if err := c.ShouldBind(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})

		return
	}

	vm := &models.VM{
		UUID: uuid.New().String(),
		Name: req.Name,
	}

	if err := orm.Create(vm).Error; err != nil {
		c.JSON(http.StatusOK, gin.H{
			"error": fmt.Errorf(errVMQueryFailed, err),
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": vm,
	})
}

func VMGet(orm *database.ORM, c *gin.Context) {
	var req uuidRequest
	if err := c.ShouldBindUri(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	var vm models.VM
	if err := orm.Where("uuid = ?", req.UUID).Preload("Properties").First(&vm).Error; err != nil {
		c.JSON(http.StatusOK, gin.H{
			"error": fmt.Errorf(errVMQueryFailed, err),
		})

		return
	}

	if vm.UUID != "" {
		c.JSON(http.StatusOK, gin.H{
			"data": vm,
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "VM not found",
	})
}

func VMDelete(orm *database.ORM, c *gin.Context) {
	var req uuidRequest
	if err := c.ShouldBindUri(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	var vm models.VM
	if err := orm.Where("uuid = ?", req.UUID).Preload("Properties").First(&vm).Error; err != nil {
		c.JSON(http.StatusOK, gin.H{
			"error": fmt.Sprintf(errVMQueryFailed, err),
		})
	}

	if vm.UUID != "" {
		if err := orm.Delete(&vm).Error; err != nil {
			c.JSON(http.StatusOK, gin.H{
				"error": fmt.Errorf(errVMDeleteFailed, err),
			})

			return
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "success",
	})
}
