package models

type VM struct {
	UUID       string       `gorm:"primary_key;unique;type:varchar(36)" json:"uuid"`
	Name       string       `gorm:"unique" json:"name"`
	Properties []VMProperty `gorm:"foreignkey:VMUUID"`
}

func (vm *VM) TableName() string {
	return "vm"
}
