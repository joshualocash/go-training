package models

type VMProperty struct {
	ID     int    `gorm:"primary_key"`
	VMUUID string `gorm:"column:vm_uuid"`
	Key    string
	Value  string
}

func (vmp *VMProperty) TableName() string {
	return "vm_property"
}
