package database

type configuration struct {
	Host     string
	Port     int
	User     string
	Name     string
	Password string
	SSL      string
}

type option func(*configuration)

func WithHost(host string) option {
	return func(c *configuration) {
		c.Host = host
	}
}

func WithPort(port int) option {
	return func(c *configuration) {
		c.Port = port
	}
}

func WithUser(user string) option {
	return func(c *configuration) {
		c.User = user
	}
}

func WithName(name string) option {
	return func(c *configuration) {
		c.Name = name
	}
}

func WithPassword(password string) option {
	return func(c *configuration) {
		c.Password = password
	}
}

func WithSSLMode(ssl string) option {
	return func(c *configuration) {
		c.SSL = ssl
	}
}
