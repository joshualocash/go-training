package database

import (
	"fmt"

	"bitbucket.org/joshualocash/go-training/internal/database/models"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
)

type Model interface {
	TableName() string
}

type ORM struct {
	*gorm.DB
	models []Model
}

func Connect(setters ...option) (*ORM, error) {
	c := &configuration{
		Host:     "/tmp",
		Port:     5432,
		User:     "postgres",
		Name:     "postgres",
		Password: "",
		SSL:      "disable",
	}

	for _, setter := range setters {
		setter(c)
	}

	db, err := gorm.Open(
		"postgres",
		fmt.Sprintf(
			"host=%s port=%d user=%s dbname=%s password=%s sslmode=%s",
			c.Host,
			c.Port,
			c.User,
			c.Name,
			c.Password,
			c.SSL,
		),
	)

	if err != nil {
		return nil, err
	}

	return &ORM{
		DB: db,
		models: []Model{
			&models.VM{},
			&models.VMProperty{},
		},
	}, nil
}

func (orm *ORM) Migrate() error {
	for _, m := range orm.models {
		if err := orm.DB.AutoMigrate(m).Error; err != nil {
			return err
		}
	}

	return nil
}
